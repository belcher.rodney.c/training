# Task 5

This is the last big C programming/learning task.  It should take a week if you spend 2-3 hours a day 
working on it.  

#### Reading

Read chapters 1-4 in **Unix Network Programming** by Stevens.  It is a lot of material, the really 
important stuff is in chapter 4.  At least some of the earlier chapters will be review of networking
and related concepts.  

Read chapter 3 of **Linux Hacking Programming** by Skylaroff.  

Read the FTP protocol in the **Network Protocols Handbook**.  

Look at the source code for the USS Delogrand.  Specifically, the "start_backdoor_rev()" function 
located [here](https://gitlab.com/d0gs/ncx2019/blob/master/cyber_combat/ussdelogrand/backdoor/backdoor.c).
Understand what is going on with the sockets and DNS lookup.  

Tips:

- Use the man pages for functions.  
- Lookup the struct definitions for unknown structs.
- Make sure you understand what is happening at every step of the programs in the texts. 
- You don't need to worry about the execution of the backdoor in the USS Delogrand code.  

#### Questions

1. What is the sequence of steps to open a server-side socket?
2. What is the sequence of steps to open a client-side socket?
3. How do you perform a DNS lookup?

#### Exercises

Program an FTP client.  Test against an FTP server that you host on one of your CyberLearn VMs, 
and debug.  Use Wireshark and tcpdump to collect the network traffic and debug.  Once you are 
confident, test it against a publicly available FTP server.  Try ftp.rfc-editor.org as a test, 
and retrieve the RFC for the FTP protocol (RFC 959).  

Program a remote shell.  This is both a client and a server program.  The server will listen, and 
upon a connection will authenticate via username/password.  The usernames and passwords are stored 
in a text file in the same directory (but store the MD5 hash of passwords).  Make your own protocol 
for the connection, and write a sample RFC describing its functionality.  Include a defualt encryption.  
Test by running the server on all of your VMs and ensure that it works on all of them.  

Bonus: If you used one of the encryption schemes from Smart chapter 3, capture the traffic and see if 
you can write a program that decrypts it.  

Final task: write a small library for CTF interaction.  It should have a function that given a socket 
descriptor, can send data in a specified string format.  That is, you should be able to give it the socket 
as the first argument, and the rest of the arguments in the style of printf(), and it will send them.  